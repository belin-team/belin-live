#!/bin/bash
zpool create -d -O mountpoint=/none -O canmount=off -R /target -o feature@allocation_classes=enabled -o ashift=12 -o autotrim=on -o feature@async_destroy=enabled      -o feature@bookmarks=enabled          -o feature@embedded_data=enabled      -o feature@empty_bpobj=enabled        -o feature@enabled_txg=enabled        -o feature@extensible_dataset=enabled -o feature@filesystem_limits=enabled -o feature@hole_birth=enabled         -o feature@large_blocks=enabled       -o feature@lz4_compress=enabled -o feature@project_quota=enabled      -o feature@resilver_defer=enabled     -o feature@spacemap_histogram=enabled -o feature@spacemap_v2=enabled        -o feature@userobj_accounting=enabled -o feature@zpool_checkpoint=enabled -O compress=lz4  bpool /dev/sda2
zpool create -f  -o ashift=12 -o autotrim=on         -O acltype=posixacl       -O relatime=on            -O xattr=sa               -O dnodesize=legacy       -O normalization=formD    -O mountpoint=none        -O canmount=off           -O devices=off            -R /target                   -O compression=zstd        rpool /dev/sda3
zfs create -o canmount=off -o mountpoint=none bpool/BOOT
zfs create -o canmount=off -o mountpoint=none rpool/ROOT
zfs create -o canmount=on -o mountpoint=/ rpool/ROOT/debian
zfs create rpool/ROOT/debian/home 
zfs create rpool/ROOT/debian/root 
zfs create -o canmount=on -o mountpoint=/boot bpool/BOOT/debian

