#!/bin/sh
export ACCESSIBILITY_ENABLED=1
export GNOME_ACCESSIBILITY=1
export GTK_OVERLAY_SCROLLING=0
export GTK_MODULES=gail:atk-bridge:libcanberra-gtk-module
export DERIVATIVE_NAME=BeLin
export QT_ACCESSIBILITY=1
export QT_LINUX_ACCESSIBILITY_ALWAYS_ON=1
export QT_IM_MODULE=xim
export DCONF_PROFILE=keybinding
export XMODIFIERS=@im=xim
if [ -d "$HOME/.gconf/apps/metacity" ] ; then
    rm -r $HOME/.gconf/apps/metacity
fi
setxkbmap -layout "hu,us,brai" -option "grp:shifts_toggle"

for i in Desktop/*
do
if [[ -f $i ]]; then
chmod +x "$i"
gio set "$i" "metadata::trusted" yes
fi
done
