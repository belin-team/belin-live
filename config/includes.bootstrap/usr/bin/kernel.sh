#!/bin/sh
set -e
apt-get -qq update
apt-get install linux-image-generic linux-headers-generic
arch=$(dpkg --print-architecture)
version=`dpkg-query -W --showformat='${VERSION}'`|grep linux-headers-common
echo kernelverzió: $version
ln -sf /usr/src/linux-headers-$version-common/include/linux/ /usr/src/linux-headers-$version-amd64/include/linux
case "$arch" in
amd64)
apt-get install libzfs4linux,libzfsbootenv1linux,zfs-initramfs,zfs-zed,zfsutils-linux fwupd-amd64-signed grub-efi-amd64-signed grub-pc grub-common grub-pc-bin grub2-common linux-headers linux-image linux-headers-amd64 shim-signed shim-helpers-amd64-signed ;;
i386)
apt-get install libzfs4linux,libzfsbootenv1linux,zfs-initramfs,zfs-zed,zfsutils-linux grub-efi-ia32-signed grub-pc linux-image-686-pae linux-headers-686-pae fwupd-i386-signed shim-signed  shim-helpers-i386-signed grub-pc grub-common grub-pc-bin grub2-common;;
esac

