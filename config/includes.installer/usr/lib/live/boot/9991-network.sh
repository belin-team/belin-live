#!/bin/bash
set -e
rm /etc/resolv.conf
ln -s /run/resolvconf/resolv.conf /etc/resolv.conf
