from debian:stable
env DEBIAN_FRONTEND=noninteractive
env TZDATA=Europe/Budapest
user root
run apt-get update && \
apt-get -y dist-upgrade && \
apt-get -y install live-build live-config-systemd git live-config live-tools joe nano zstd && \
apt-get clean && \
apt-get autoclean && \
mkdir /belin-live && \
ln -sf /usr/share/zoneinfo/Europe/Budapest /etc/localtime && \
cd /usr/share/live/build/data/debian-cd && \
ln -s squeeze bookworm && \
cd /
add etc /etc
workdir /belin-live
run git clone https://salsa.debian.org/belin-team/belin-live.git
workdir /belin-live/belin-live
run git pull && \
cp ./build.sh /usr/bin && \
chmod +x /usr/bin/build.sh

