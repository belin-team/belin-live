#!/bin/bash
set -e
#lb clean --all
#lb clean --purge
#rm -rf cache
#arch=`dpkg -a`
i=1;
for param in "$@"
do
echo $param
done
#auto/config
lb config $1 $2 "$3" "$4" $5
kernel=`uname -r`
#echo $kernel
#echo $2
common=`uname -r |sed 's/amd64/common/'`
#echo $kernel
#echo $common
mkdir -p chroot && pushd chroot
mkdir -p usr/src/linux-headers-$common/include/linux
mkdir -p usr/src/linux-headers-$kernel/include
echo chroot/usr/src/linux-headers-$kernel/include
#pushd ./chroot
ln -sf /usr/src/linux-headers-$common/include/linux/ usr/src/linux-headers-$kernel/include/linux
popd
#lb build
